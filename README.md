# Delete Container Cache

## Purpose

Delete cache container created by Kaliko

## Environment variables

### MANDATORY
<!-- markdownlint-disable line-length -->
| Name                   | Other Name      | Purpose                               |
|------------------------|-----------------|---------------------------------------|
| `REPO_NAME`            | N/A             | Name of the repository to clean       |
| `GITLAB_PRIVATE_TOKEN` | N/A             | Token to use to manipulate repository |
| `GITLAB_MAIN_URL`      | `CI_SERVER_URL` | Gitlab base url                       |
| `PROJECT_ID`           | `CI_PROJECT_ID` | ID of the project                     |
<!-- markdownlint-enable line-length -->

### Usage

Add into CI/CD variables of your project the sensitive value
(`GITLAB_PRIVATE_TOKEN`).

Create a step in your CI with required values:

```yaml
variables:
  CHAINED_CI_PROJECT_ID: 1234

delete_cached_layers:
  stage: build
  tags:
    - kubernetes
  variables:
    REPO_NAME: "the_repo_to_clean"
  script:
    - ./delete-container-cache.py
```
