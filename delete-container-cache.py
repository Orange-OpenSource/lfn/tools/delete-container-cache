#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0

import os
import sys
import gitlab
from loguru import logger

gitlab_main_url = os.getenv('GITLAB_MAIN_URL', os.getenv('CI_SERVER_URL'))
bad_vars = False

private_token = os.getenv('GITLAB_PRIVATE_TOKEN')
if not private_token:
  logger.error(
    "/!\ Environment variable 'GITLAB_PRIVATE_TOKEN' must be given",
  )
  logger.error("      It's used to manipulate project registry.")
  bad_vars = True

project_id = os.getenv('PROJECT_ID', os.getenv('CI_PROJECT_ID'))
if not project_id:
  logger.error("/!\ Environment variable 'PROJECT_ID' must be given")
  logger.error("      It's used to know on which project we work.")
  bad_vars = True

repository_name = os.getenv('REPO_NAME')
if not repository_name:
  logger.error("/!\ Environment variable 'REPO_NAME' must be given")
  logger.error("      It's used to know which repository we must delete.")
  bad_vars = True

if bad_vars:
  logger.error(
    "/!\ At least one mandatory environment variable is not given, exiting.",
  )
  sys.exit(os.EX_USAGE)

logger.debug("Creating gitlab client")
client = gitlab.Gitlab(gitlab_main_url, private_token=private_token)
logger.debug("Retrieving project id {}", project_id)
project = client.projects.get(project_id)
if not project:
  logger.error("/!\ issue will retrieving chained ci project, exiting")
  sys.exit(os.EX_UNAVAILABLE)

logger.debug(
  "Retrieving container repositories in order to find {}",
  repository_name,
)
for repository in  project.repositories.list(all=True):
  logger.debug("* repository: {}", repository.name)
  if repository_name == repository.name:
    logger.info("repository ({}) found, deleting it", repository_name)
    repository.delete()
    sys.exit(os.EX_OK)
