FROM python:3.9-alpine
LABEL org.opencontainers.image.source ="https://gitlab.com/Orange-OpenSource/lfn/tools/delete-container-cache"
LABEL org.opencontainers.image.description="Delete container cache docker image"
LABEL org.opencontainers.image.authors="Sylvain Desbureaux <sylvain.desbureaux@orange.com>"

ARG USER=user
ARG SRC=/usr/src/app/delete-container-cache

ENV CRYPTOGRAPHY_DONT_BUILD_RUST=1
ENV HOME /home/$USER

COPY requirements.txt $SRC/
WORKDIR $SRC

RUN adduser -D $USER &&\
      pip install --no-cache-dir -r requirements.txt && \
      rm -rf /var/lib/apt/lists/* /tmp/* &&\
      rm -rf ~/.cache/pip

COPY delete-container-cache.py $SRC/

USER $USER
WORKDIR $HOME
CMD ["./delete-container-cache.py"]
